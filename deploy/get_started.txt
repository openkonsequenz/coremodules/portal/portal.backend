>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
GET STARTED
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


---------------- REQUIREMENTS -------------
Java 1.8
Apache Maven 3.5.0

IF you have this versions, check if you have set JAVA_HOME and M2_HOME.
    - If yes ignore step 1 and 2
    - If no follow the corresponding steps below

-------------------- 1 ---------------------
Install java 8

- Open a cmd and check your java version
    $ java -version
- If you dont have java version 1.8 then download and install it from:
        http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- Set JAVA_HOME (Windows OS):
    Right click on This Computer and select Properties.
    On the Advanced tab, select Environment Variables, and then edit JAVA_HOME to point to where the JDK software is located, for example, C:\Program Files\Java\jdk1.8.xxx.
    Also, edit The Path variable in System variables and add the bin path (C:\Program Files\Java\jdk1.8.xxx\bin)
- Restart your PC
- Confirm by checking the version
    $ java -version


-------------------- 2 ---------------------
Install Apache Maven to your pc

Maven is a tool that can be used for building and managing any Java-based project.

- Download maven 3.5.0 from
        https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.5.0/
- Extract the folder and place it on your C drive
- Set M2_HOME (Windows OS):
    - Right click on This Computer and select Properties.
    - On the Advanced tab, select Environment Variables, and then create M2_HOME to point to where the Maven software is located, for example, C:\apache-maven-3.5.0.
    - Also edit The Path variable in System variables and add the bin path (C:\apache-maven-3.5.0\bin)
- Restart your PC
- Confirm with
    $ mvn -v

-------------------- 3 ---------------------
To get started with the project you have to clone all four(elogbook and portal backend-frontend ) projects.

- Create a folder and do following for each of them
- Open a cmd and navigate to the folder
- Use the command to create a clone of each project
    § git clone <project_url>
- If you are going to develop, important is a switch to the develop branch
    - navigate in the folder with the clone and switch to develop branch
        $ git checkout <branch>
- At the end just pull the actual version of the code
    $ git pull origin <branch>


--------------------------
Active Develop Branches: |
Backend -> DEVELOP_BE    |
Frontend -> DEVELOP_FE   |
-------------------------|
Stable Branches: master  |
--------------------------


-------------------- 4 ---------------------
Build the maven project

Building the maven project, will produce a <project_name>.war file(Web Application Archive), which is needed to run the project.
- Open a command line and navigate to the root folder of the backend project
- Use the following call to cleanly build and install artifacts into the local repository:
    $ mvn clean install

-------------------- 5 ---------------------
Open and familiarize yourself with the project

- If you successfully build the maven project you can find in /target/generated-docs all the documentations of this projects
    - auth_n_auth_howtoBuild -> here you can read how to build the whole (backend and frontend) project
    - auth_n_auth_howtoRun -> here you can read how to run the Application
    - auth_n_auth_architectureDocumentation -> here you can read about the architecture documetation this module
    - auth_n_auth_interfaceDocumentation -> here you can read about the backend REST Services which are used

- Other useful files:
    - /target/site/jacoco/index.html -> jacoco code coverage review
    - /target/surefire-reports -> test reports

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
NEXT STEP -> READ THE auth_n_auth_howtoBuild FILE !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!