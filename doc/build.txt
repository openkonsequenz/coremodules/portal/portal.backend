>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Backend:
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Voraussetzungen:
----------------
Java 1.8 JDK
Maven

Bauen:
------
Im Rootpath des Backend-Projektes:
>mvn clean install

(Erzeugt in Target das WAR, die surefire-reports und das jacoco.exec f�r die Codecoverage-Analyse)
Im Sonarqube schlie�en wir folgende Files von der CodeCoverageAnalyse aus:


**/de/pta/openkonsequenz/betriebstagebuch/common/Globals.java
**/rest/*.java
**/LoggerUtil.java
**/dao/*.java
**/dao/interfaces/*.java
