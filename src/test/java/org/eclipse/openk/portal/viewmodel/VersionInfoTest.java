/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.viewmodel;


import static org.junit.Assert.assertTrue;

import org.eclipse.openk.portal.common.JsonGeneratorBase;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.junit.Test;

public class VersionInfoTest extends ResourceLoaderBase {
    // IMPORTANT TEST!!!
    // Make sure, our Interface produces a DEFINED Json!
    // Changes in the interface will HOPEFULLY crash here!!!

    @Test
    public void testStructureAgainstJson() {
        String json = super.loadStringFromResource("VersionInfo.json");
        VersionInfo vi = JsonGeneratorBase.getGson().fromJson(json, VersionInfo.class);

        assertTrue(vi.getBackendVersion().equals("0x.0x.xx"));
    }

    @Test
    public void testSetters() {
        VersionInfo vi = new VersionInfo();

        vi.setBackendVersion("222");
    }

}
