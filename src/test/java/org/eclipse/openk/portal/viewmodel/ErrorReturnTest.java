/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.viewmodel;

import static org.eclipse.openk.portal.common.JsonGeneratorBase.getGson;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.junit.Test;

public class ErrorReturnTest extends ResourceLoaderBase {
	// IMPORTANT TEST!!!
	// Make sure, our Interface produces a DEFINED Json!
	// Changes in the interface will HOPEFULLY crash here!!!

	@Test
	public void TestStructureAgainstJson() {
		String json = super.loadStringFromResource("testErrorReturn.json");
		ErrorReturn errRet = getGson().fromJson(json, ErrorReturn.class);
		assertFalse(errRet.getErrorText().isEmpty());
		assertEquals(999, errRet.getErrorCode());
	}

	@Test
	public void TestSetters() {
		ErrorReturn errRet = new ErrorReturn();
		errRet.setErrorCode(1);
		errRet.setErrorText("bla bla");
	}

}
