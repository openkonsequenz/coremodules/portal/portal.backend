/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.controller;

import org.eclipse.openk.portal.common.Globals;
import org.eclipse.openk.portal.common.util.ResourceLoaderBase;
import org.eclipse.openk.portal.exceptions.PortalBadRequest;
import org.eclipse.openk.portal.exceptions.PortalUnauthorized;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class InputValidatorTest extends ResourceLoaderBase {

    // Check Credentials -------------------------------------------------------

    @Test(expected = PortalUnauthorized.class)
    public void testCheckCredentials_Null() throws PortalUnauthorized {
        new InputDataValuator().checkCredentials(null);
    }

    @Test(expected = PortalUnauthorized.class)
    public void testCheckCredentials_Empty() throws PortalUnauthorized {
        new InputDataValuator().checkCredentials("");
    }

    @Test(expected = PortalUnauthorized.class)
    public void testCheckCredentials_TooLong() throws PortalUnauthorized {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Globals.MAX_CREDENTIALS_LENGTH; i++) {
            sb.append("W");
        }
        new InputDataValuator().checkCredentials(sb.toString());

    }

    @Test(expected = PortalUnauthorized.class)
    public void testCheckCredentials_Invalid() throws PortalUnauthorized {
        new InputDataValuator().checkCredentials("{\"abc\":\"123\"}");
    }

    @Test(expected = PortalUnauthorized.class)
    public void testCheckCredentials_InvalidNoJson() throws PortalUnauthorized {
        new InputDataValuator().checkCredentials("123");
    }

    @Test(expected = PortalUnauthorized.class)
    public void testCheckCredentials_tooLong() throws PortalUnauthorized {
        StringBuilder sb = new StringBuilder(Globals.MAX_CREDENTIALS_LENGTH + 1);
        for (int i = 0; i < Globals.MAX_CREDENTIALS_LENGTH + 1; i++) {
            sb.append("@");
        }
        new InputDataValuator().checkCredentials(sb.toString());

    }

    @Test
    public void checkWhitelistChars_ok() throws Exception {
        callWhitelistCheck("AaBbCcäÄöÖüÜß ?0123456789(). _+  ,-:;=!§%&/'#<>\"");
    }

    @Test(expected = PortalBadRequest.class)
    public void checkWhitelistChars_nok() throws Exception {
        callWhitelistCheck(null);
        callWhitelistCheck("");
        callWhitelistCheck("AaBbCc.,{}");
    }

    private void callWhitelistCheck(String tst) throws Exception {
        InputDataValuator idv = new InputDataValuator();
        Whitebox.invokeMethod(idv, "checkWhitelistChars", tst, false);
    }


}
