/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.auth2.model;

public class KeyCloakRole {

  private String id;
  private String name;
  private Boolean scopeParamRequired;
  private Boolean composite;
  private Boolean clientRole;
  private String containerId;

  public KeyCloakRole(String id, String name, Boolean scopeParamRequired, Boolean composite, Boolean clientRole, String containerId) {
    this.id = id;
    this.name = name;
    this.scopeParamRequired = scopeParamRequired;
    this.composite = composite;
    this.clientRole = clientRole;
    this.containerId = containerId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getScopeParamRequired() {
    return scopeParamRequired;
  }

  public void setScopeParamRequired(Boolean scopeParamRequired) {
    this.scopeParamRequired = scopeParamRequired;
  }

  public Boolean getComposite() {
    return composite;
  }

  public void setComposite(Boolean composite) {
    this.composite = composite;
  }

  public Boolean getClientRole() {
    return clientRole;
  }

  public void setClientRole(Boolean clientRole) {
    this.clientRole = clientRole;
  }

  public String getContainerId() {
    return containerId;
  }

  public void setContainerId(String containerId) {
    this.containerId = containerId;
  }
}
