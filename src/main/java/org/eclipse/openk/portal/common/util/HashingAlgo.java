/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
package org.eclipse.openk.portal.common.util;

import java.security.MessageDigest;
import org.apache.commons.codec.binary.Hex;

public final class HashingAlgo {
    private HashingAlgo() {}

    public static String hashIt(String pwdClear) {
        MessageDigest cript;
        try {
            cript = MessageDigest.getInstance("SHA-1");
            cript.reset();
            cript.update(pwdClear.getBytes("utf8"));
            return new String(Hex.encodeHex(cript.digest()));

        } catch (Exception e) { //NOSONAR
            return null;
        }
    }
}
